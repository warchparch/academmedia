
from django.conf.urls import url

from .views import login,logout,register,registration_complete

urlpatterns = [
    url(r'^register/$', register, name='register'),
    url(r'^register/complete/$', registration_complete,
        name='registration_complete'),
    url(r'^login/$', login),
    url(r'^logout/$', logout),

]
