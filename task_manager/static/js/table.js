function getCookie(name) {
          var cookieValue = null;
          if (document.cookie && document.cookie != '') {
                var cookies = document.cookie.split(';');
          for (var i = 0; i < cookies.length; i++) {
               var cookie = jQuery.trim(cookies[i]);
          // Does this cookie string begin with the name we want?
          if (cookie.substring(0, name.length + 1) == (name + '=')) {
            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
              break;
             }
          }
      }
 return cookieValue;
}

var csrftoken = getCookie('csrftoken');

function validateTable() {{
    var result = true;
    var task = document.getElementById('edit_task').value;
    var performer = document.getElementById('edit_selection').value;
        if(!task&&!performer){
        result = false;
        alert('Необходимо выбрать исполнителя и указать задание')
    }
    else if(!task&&performer){
            result = false;
            alert('Необходимо указать задание')
        }
            else if(task&&!performer){
            result = false;
            alert('Необходимо выбрать исполнителя')
        }
    else{

        add_to_table([task,performer])
    }
    return result;

}}

function add_to_table(NewData) {


    $.ajax({
        type: "POST",
        url: '/table/add/',
        data: {
            csrfmiddlewaretoken : csrftoken,
            array: JSON.stringify(NewData)
        },
        success: function (response) {
            if (response.status === "OK") {
                var table = document.getElementById('MyTable');
                var new_row = document.createElement('tr');
                var last_id = response.last_id;
                var td1 = document.createElement('td'); td1.innerHTML = NewData[0];
                var td2 = document.createElement('td'); td2.innerHTML = response.creator;
                var td3 = document.createElement('td'); td3.innerHTML = NewData[1];
                var td4 = document.createElement('td'); td4.innerHTML = response.time;
                var td5 = document.createElement('td'); td5.innerHTML = "<td><div id='td" + last_id + "'></div></td>";
                var td6 = document.createElement('td'); td6.innerHTML = '<button id="bt' + String(last_id) + '" class = login_button  type="submit" onclick="return commit_table(' + last_id + ');">Завершить задачу</button>';
                new_row.appendChild(td1);
                new_row.appendChild(td2);
                new_row.appendChild(td3);
                new_row.appendChild(td4);
                new_row.appendChild(td5);
                new_row.appendChild(td6);

                var last = document.getElementById('addRow');
                table.appendChild(new_row);
                table.insertBefore(new_row,last);
                document.getElementById('edit_task').value = '';
                document.getElementById('edit_selection').value = '';



            }
            else {
                console.log('All is not fine')
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("Doesn't work")

        }
    });

}

function commit_table(NewData) {
    console.log(NewData);
    $.ajax({
        type: "POST",
        url: '/table/commit/',
        data: {
            csrfmiddlewaretoken : csrftoken,
            array: JSON.stringify(NewData)
        },
        success: function (response) {
            if (response.status === "OK") {
                console.log('All is fine');
                document.getElementById('td'+String(NewData)).innerHTML = response.time;
                var remove = document.getElementById('bt'+String(NewData));
                remove.remove()
            }
            else {
                console.log('All is not fine')
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("Doesn't work")

        }
    });

}


function filterr(phrase, _id, cellNumber, depend, resetFilter) {
   var words = phrase.value.toLowerCase().split(" ");
   var table = document.getElementById(_id);
   cycle: for (var r = 0; r < table.rows.length - 1; r++) {
      console.log('2 col:',table.rows[r].style.display);

      var cellsV = table.rows[r].cells[cellNumber].innerHTML.replace(/<[^>]+>/g, "");

      var displayStyle = 'none';
      for (var i = 0; i < words.length; i++) {
         if (cellsV.toLowerCase().indexOf(words[i]) >= 0)
            displayStyle = '';
         else {
            displayStyle = 'none';
            break;
         }
      }
      table.rows[r].cells[cellNumber].style.display = displayStyle;
            if (!resetFilter) {
         for (i in (depend)) {
             console.log(depend[i]);
             if (table.rows[r].cells[depend[i]].style.display === 'none') {
                 continue cycle;
             }
         }
      }
      table.rows[r].style.display = displayStyle;
   }
}

  $( function() {
    $( "#datepicker" ).daterangepicker();
  } );