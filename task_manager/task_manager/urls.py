from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^', include('task_table.urls', namespace='main')),
    url(r'^table/', include('task_table.urls', namespace='task_table')),
    url(r'^admin/', admin.site.urls),
    url(r'^auth/', include('loginsys.urls', namespace='login')),
]