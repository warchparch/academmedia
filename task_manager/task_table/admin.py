from django.contrib import admin
from .models import Task

class TaskAdmin(admin.ModelAdmin):
    list_display = ['task','creator','performer','created','solved']
    list_filter = ['creator','performer']
    # list_editable = ['task','creator','performer']
    # prepopulated_fields = {'slug': ('name', )}

admin.site.register(Task, TaskAdmin)