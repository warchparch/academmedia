from django.apps import AppConfig


class TaskTableConfig(AppConfig):
    name = 'task_table'
