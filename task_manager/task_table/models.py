from django.db import models

class Task(models.Model):
    task = models.CharField(max_length=200, db_index=True, verbose_name="Задача")
    creator = models.CharField(max_length=200, db_index=True, verbose_name="Создатель")
    performer = models.CharField(max_length=200, db_index=True, verbose_name="Исполнитель")
    created = models.DateTimeField(auto_now_add=True)
    solved = models.DateTimeField(blank=True,null=True)

