from . import views

from django.conf.urls import url


urlpatterns = [

    url(r'^$', views.start_page, name='start_page'),
    url(r'^table/commit/$', views.CommitRowView.as_view(), name='commit'),
    url(r'^table/add/$', views.AddRowView.as_view(), name='add'),
    url(r'^table/$', views.table, name='table'),

]