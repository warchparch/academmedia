from django.shortcuts import render, redirect
from .models import Task
from django.contrib.auth.models import User
from django.views import generic
from django.http import JsonResponse
import json
import datetime





def start_page(request):
    if request.user.is_authenticated:
        return redirect('/table/')
    else:
        return render(request, 'start_page.html',)


def table(request):
    if request.user.is_authenticated:
        table = Task.objects.all()
        users = User.objects.exclude(groups__name='admin')

        return render(request, 'table.html', {'table': table,'users':users}, )
    else:
        return render(request, 'start_page.html')



def time_now():
    time = datetime.datetime.now()
    time = time.strftime('%B %d, %Y, %I:%M %p')
    return time.replace('AM', 'a.m.').replace('PM','p.m.')


class AddRowView(generic.View):
    def post(self, request):
        post = request.POST
        adding = json.loads(post.get('array'))
        creator = request.user.username
        new_entry = Task(task=adding[0], creator=creator, performer=adding[1])
        new_entry.save()
        last_id = Task.objects.latest('id')

        return JsonResponse({'status': 'OK','creator':creator,'time':time_now(),'last_id':last_id.id})


class CommitRowView(generic.View):
    def post(self, request):
        post = request.POST
        adding = json.loads(post.get('array'))
        new_entry = Task.objects.get(id=adding)
        new_entry.solved = datetime.datetime.now()
        new_entry.save()
        return JsonResponse({'status': 'OK','time':time_now()})


